package com.example.listado_de_cartas.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.example.listado_de_cartas.R;

public class DetailActivityFragment extends Fragment {


    private View view;
    private ImageView ivPosterImage;
    private TextView tvName;
    private TextView tvRarity;
    private TextView tvArtist;
    private TextView tvType;
    private TextView tvText;

    public static DetailActivityFragment newInstance() {return  new DetailActivityFragment();}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Cartas carta = (Cartas) i.getSerializableExtra("card");

            if (carta != null) {
                updateUi(carta);
            }
        }
        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(getViewLifecycleOwner(), new Observer<Cartas>() {
            @Override
            public void onChanged(@Nullable Cartas carta) {
                updateUi(carta);
            }
        });
        return view;
    }

    private void updateUi(Cartas carta) {
        Log.d("CARD", carta.toString());

        ivPosterImage = view.findViewById(R.id.ivPosterImage);
        tvName = view.findViewById(R.id.tvName);
        tvRarity = view.findViewById(R.id.tvRarity);
        tvArtist = view.findViewById(R.id.tvArtist);
        tvType = view.findViewById(R.id.tvType);
        tvText = view.findViewById(R.id.tvText);

        tvName.setText(carta.getName());
        tvRarity.setText(carta.getRarity());
        tvArtist.setText(carta.getArtist());
        tvType.setText(carta.getType());
        tvText.setText(carta.getText());

        Glide.with(getContext()).load(
                carta.getImageUrl()
        ).into(ivPosterImage);
    }
}
