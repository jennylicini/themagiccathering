package com.example.listado_de_cartas.ui.main;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.listado_de_cartas.R;

import java.util.ArrayList;
import java.util.Arrays;

public class MainFragment extends Fragment {


    private ListView lvCartas;
    public CartasAdapter adapter; //es el puente entre los datos y las vistas
    private CartasViewModel model;
    private SharedPreferences preferences;
    private SharedViewModel sharedModel;
    private MainViewModel mViewModel;
    public static String raredadpublica;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.main_fragment, container, false);


        //Para saber cuales carats mostrar
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        raredadpublica=sp.getString("Raredad", "Raredad");


        ArrayList<Cartas> items= new ArrayList<>();
        adapter=new CartasAdapter(
                getActivity(),
                R.layout.lv_cartas_row,
                items
        );

        sharedModel = ViewModelProviders.of(getActivity()).get(
                SharedViewModel.class
        );

        lvCartas=view.findViewById(R.id.lvCartas); // encuentro la vista del listview
        lvCartas.setAdapter(adapter);

        lvCartas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cartas carta = (Cartas) adapterView.getItemAtPosition(i);
                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("card", carta);
                    startActivity(intent);
                } else {
                    sharedModel.select(carta);
                }
            }
        });

        model = ViewModelProviders.of(this).get(CartasViewModel.class);

        model.getCartas().observe(getViewLifecycleOwner(), cartas -> {
            adapter.clear();
            adapter.addAll(cartas);
        });
        return view;

    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cartas>> {
        @Override
        protected ArrayList<Cartas> doInBackground(Void... voids) {
            CartasDBAPI api = new CartasDBAPI();
            ArrayList<Cartas> result = api.getCartas();
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cartas> cartas) {
            adapter.clear();
            for (Cartas card: cartas) {
                adapter.add(card);
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas_fragment, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
        model.reload();
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

}