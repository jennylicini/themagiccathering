package com.example.listado_de_cartas.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Cartas> selected = new MutableLiveData<Cartas>();

    public void select(Cartas cartas) {
        selected.setValue(cartas);
    }

    public LiveData<Cartas> getSelected() {
        return selected;
    }
}
