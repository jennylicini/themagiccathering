package com.example.listado_de_cartas.ui.main;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CartasDBAPI {

    private final String BASE_URL = "https://api.magicthegathering.io";


    ArrayList<Cartas> getCartas() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    private ArrayList<Cartas> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Cartas> processJson(String jsonResponse) {
        ArrayList<Cartas> cartas = new ArrayList<>();
        String rarity = MainFragment.raredadpublica;

        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCartas = data.getJSONArray("cards");

            for (int i = 0; i < jsonCartas.length(); i++) {
                JSONObject jsonCarta = jsonCartas.getJSONObject(i);
                Cartas carta = new Cartas();
                String raredad = jsonCarta.getString("rarity");

                switch (rarity){
                    case "0":
                        addCarta(carta, jsonCarta, cartas, raredad);
                        break;
                    case "1":
                        if (raredad.equals("Rare")){
                            addCarta(carta, jsonCarta, cartas, raredad);
                        }
                        break;
                    case "2":
                        if (raredad.equals("Common")){
                            addCarta(carta, jsonCarta, cartas, raredad);
                        }
                        break;
                    case "3":
                        if (raredad.equals("Uncommon")){
                            addCarta(carta, jsonCarta, cartas, raredad);
                        }
                        break;
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        imprimirArraylist(cartas);
        return cartas;
    }

    private void addCarta(Cartas carta, JSONObject jsonCarta, ArrayList<Cartas> cartas, String raredad) throws JSONException {
        carta.setRarity(raredad);
        carta.setName(jsonCarta.getString("name"));
        carta.setType(jsonCarta.getString("type"));
        carta.setArtist(jsonCarta.getString("artist"));
        carta.setText(jsonCarta.getString("text"));
        if (jsonCarta.has("imageUrl") ){
            carta.setImageUrl(jsonCarta.getString("imageUrl"));
        } else {
            carta.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/71egy8NXeVL.jpg");
        }
        cartas.add(carta);
    }


    private void imprimirArraylist(ArrayList<Cartas> cartas) {

    }

}
