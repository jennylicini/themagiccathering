package com.example.listado_de_cartas.ui.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.listado_de_cartas.R;

import java.util.List;

public class CartasAdapter extends ArrayAdapter<Cartas> {
    public CartasAdapter(Context context, int resource, List<Cartas> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtenim l'objecte en la possició corresponent
        Cartas carta = getItem(position);


        // Mirem a veure si la View s'està reutilitzant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cartas_row, parent, false);
        }

        // Unim el codi en les Views del Layout
        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvRarity = convertView.findViewById(R.id.tvRarity);
        ImageView ivPosterImage = convertView.findViewById(R.id.ivPosterImage);

        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        tvName.setText(carta.getName());
        tvRarity.setText(carta.getRarity());

        Glide.with(getContext()).load(
                carta.getImageUrl()
        ).into(ivPosterImage);

        // Retornem la View replena per a mostrar-la
        return convertView;
    }

}
