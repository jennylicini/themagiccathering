package com.example.listado_de_cartas.ui.main;

import android.os.Bundle;

import com.example.listado_de_cartas.R;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        System.out.println("DETAILACTIVITY");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetailActivityFragment.newInstance())
                    .commitNow();
        }


    }
}