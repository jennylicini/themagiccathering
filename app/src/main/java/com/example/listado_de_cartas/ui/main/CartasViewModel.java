package com.example.listado_de_cartas.ui.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class CartasViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDao cartaDao;
    private LiveData<List<Cartas>> cartas;

    public CartasViewModel(Application application) {
        super(application);
        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                (this.getApplication()));
        this.cartaDao =appDatabase.getCartasDao();
    }

    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public LiveData<List<Cartas>> getCartas() {
        return cartaDao.getCards();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            CartasDBAPI api=new CartasDBAPI();
            ArrayList<Cartas> result;

            result=api.getCartas();

            cartaDao.deleteCard();
            cartaDao.addCards(result);

            return null;
        }
    }



}
