package com.example.listado_de_cartas.ui.main;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CartaDao {

    @Query("select * from cartas")
    LiveData<List<Cartas>> getCards();

    @Insert
    void addCards(Cartas card);

    @Insert
    void addCards(List<Cartas> cards);

    @Delete
    void deleteCard(Cartas card);

    @Query("DELETE FROM cartas")
    void deleteCard();
}
